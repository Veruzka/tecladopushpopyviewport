package vmorales;


import com.sun.opengl.util.Animator;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;

import javax.swing.JFrame;
import com.sun.opengl.util.GLUT;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.media.opengl.glu.GLUquadric;




public class Viewport extends JFrame implements KeyListener {
 
    public static GL gl;
    public static GLU glu;
    public static GLUT glut;
    public static GLCanvas canvas;
    public static int ancho,alto;
    private static float rotarX=0;
    private static float trasladaX=0;
    private static float scaleX=3,scaleY=3;
    

     public static void main (String args[]){
         Viewport myframe = new Viewport();
         myframe.setVisible(true);
         myframe.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

     public Viewport(){
        setSize(700,600);
        setLocationRelativeTo(null);
        setTitle("Figuras con viewPort");
        setResizable(false);
        GraphicListener listener = new GraphicListener();
        alto = this.getHeight();
        ancho = this.getWidth();
        canvas= new GLCanvas();
        gl=canvas.getGL();
        glu= new GLU();
        glut = new GLUT();
        canvas.addGLEventListener(listener);
        getContentPane().add(canvas);
        Animator animator = new Animator(canvas);
        animator.start();
        addKeyListener(this);

     }

     public class GraphicListener implements GLEventListener{

        public void display(GLAutoDrawable arg0){
            
            gl=arg0.getGL();
            gl.glClear(GL.GL_COLOR_BUFFER_BIT);
            gl.glColor3f(1f, 1f, 1f);

            gl.glMatrixMode(GL.GL_PROJECTION);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);

            //para el cilindro
            GLUquadric quad;
            quad = glu.gluNewQuadric ();
            glu.gluQuadricDrawStyle(quad, GLU.GLU_FILL);
            
            //dibuja cubo 
            gl.glPushMatrix();
            gl.glViewport(0,455, 100, 100);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            gl.glColor3f(0,1,0);
            glut.glutWireCube(1);
            gl.glPopMatrix();
             
            //dibuja cubo solido
            gl.glPushMatrix();
            gl.glViewport(100,455, 100, 100);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            glut.glutSolidCube(1);
            gl.glPopMatrix();
            
             //dibuja cono
            gl.glPushMatrix();
            gl.glViewport(0,355, 100, 100);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            gl.glColor3f(0.439216f,0.858824f,0.576471f);
            glut.glutWireCone(1,1,50,5);
            gl.glPopMatrix();
            
            //dibuja cono solido
            gl.glPushMatrix();
            gl.glColor3f(0.439216f,0.858824f,0.576471f);
            gl.glViewport(100,355, 100, 100);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            glut.glutSolidCone(1,1,50,5);
            gl.glPopMatrix();
            //dibuja esfera
            gl.glPushMatrix();
            gl.glColor3f(0.62352f,0.372549f,0.623529f);
            gl.glViewport(0,255, 100, 100);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            glut.glutWireSphere(1,10,10);
            gl.glPopMatrix();
            //dibuja esfera solido
            gl.glPushMatrix();
            gl.glColor3f(0.62352f,0.372549f,0.623529f);
            gl.glViewport(100,255, 100, 100);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            glut.glutSolidSphere(1,10,10);
            gl.glPopMatrix();
            //dibuja torus
            gl.glPushMatrix();
            gl.glColor3f(1,0,0);
            gl.glViewport(0,155, 100, 100);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            glut.glutWireTorus(0.4,0.8,10,20);
            gl.glPopMatrix();
            //dibuja torus solido
            gl.glPushMatrix();
            gl.glColor3f(1,0,0);
            gl.glViewport(100,155,100,100);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            glut.glutSolidTorus(0.4,0.8,10,20);
            gl.glPopMatrix();
            //dibuja tetera
            gl.glPushMatrix();
            gl.glColor3f(0,1,0);
            gl.glViewport(0,55,100,100);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            glut.glutWireTeapot(1);
            gl.glPopMatrix();
            //dibuja tetera solida
            gl.glPushMatrix();
            gl.glColor3f(0,1,0);
            gl.glViewport(100,55,100,100);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            glut.glutSolidTeapot(1);
            gl.glPopMatrix();
             //dibuja tetraedro
            gl.glPushMatrix();
            gl.glColor3f(0,0,1);
            gl.glViewport(200,455,100,100);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            glut.glutWireTetrahedron();
            gl.glPopMatrix();
            //dibuja tetraedro solida
            gl.glPushMatrix();
            gl.glColor3f(0,0,1);
            gl.glViewport(300,455,100,100);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            glut.glutSolidTetrahedron();
            gl.glPopMatrix();
             //dibuja octaedro
            gl.glPushMatrix();
            gl.glColor3f(1,0,1);
            gl.glViewport(200,355,100,100);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            glut.glutWireOctahedron();
            gl.glPopMatrix();
            //dibuja octaedro solida
            gl.glPushMatrix();
            gl.glColor3f(1,0,1);
            gl.glViewport(300,355,100,100);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            glut.glutSolidOctahedron();
            gl.glPopMatrix();
            //dibuja icosaedro
            gl.glPushMatrix();
            gl.glColor3f(0,1,1);
            gl.glViewport(200,255,100,100);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            glut.glutWireIcosahedron();
            gl.glPopMatrix();
            //dibuja icosaedro solida
            gl.glPushMatrix();
            gl.glColor3f(0,1,1);
            gl.glViewport(300,255, 100, 100);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            glut.glutSolidIcosahedron();
            gl.glPopMatrix();
            //dibuja dodecaedro
            gl.glPushMatrix();
            gl.glColor3f(1,1,0);
            gl.glViewport(200,155, 100, 100);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            glut.glutWireDodecahedron();
            gl.glPopMatrix();
            //dibuja dodecaedro solida
            gl.glPushMatrix();
            gl.glColor3f(1,1,0);
            gl.glViewport(300,155, 100, 100);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            glut.glutSolidDodecahedron();
            gl.glPopMatrix();
            //dibuja dodecaedro
            gl.glPushMatrix();
            gl.glColor3f(0,0.5f,1);
            gl.glViewport(200,55, 100, 100);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            glut.glutWireRhombicDodecahedron();
            gl.glPopMatrix();
            //dibuja dodecaedro solida
            gl.glPushMatrix();
            gl.glColor3f(0,0.5f,1);
            gl.glViewport(300,55, 100, 100);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            glut.glutSolidRhombicDodecahedron();
            gl.glPopMatrix();
            //dibuja cilindro
            gl.glPushMatrix();
            gl.glColor3f(1,0.498039f, 0);
            gl.glViewport(400,455, 100, 100);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            glu.gluCylinder(quad, 0.5, 0.5, 1, 40, 40);
            gl.glPopMatrix();
           
            gl.glFlush();
            rotarX=rotarX+0.2f;
            
        }

         public void displayChanged(GLAutoDrawable arg0, boolean arg1, boolean arg2){
        }

        public void init(GLAutoDrawable arg0){         
        }

        public void reshape(GLAutoDrawable arg0, int arg1, int arg2, int arg3, int arg4){
        }//reshape...
     }//graphic listener


     //Creacion del metodo que detecta las teclas pulsadas
    public void keyPressed(KeyEvent arg0){
        
        if(arg0.getKeyCode()==KeyEvent.VK_RIGHT){
            trasladaX+=.10f;
            System.out.println("El valor de Z: "+trasladaX);
        }

          if(arg0.getKeyCode()==KeyEvent.VK_LEFT){
            trasladaX-=.10f;
            System.out.println("El valor de Z: "+trasladaX);
        }
         
          if(arg0.getKeyCode()==KeyEvent.VK_A){
            rotarX+=1.0f;
            System.out.println("El valor de X en la rotacion: "+rotarX);
        }

        if(arg0.getKeyCode()==KeyEvent.VK_D){
            rotarX-=1.0f;
            System.out.println("El valor de X en la rotacion: "+rotarX);
        }

       

          if(arg0.getKeyCode()==KeyEvent.VK_J){
            scaleX+=.10f;
            System.out.println("El valor de Z: "+scaleX);
            scaleY+=.10f;
            System.out.println("El valor de Z: "+scaleY);
        }

          if(arg0.getKeyCode()==KeyEvent.VK_L){
            scaleX-=.10f;
            System.out.println("El valor de Z: "+scaleX);
            scaleY-=.10f;
            System.out.println("El valor de Z: "+scaleY);
        }
         

          if(arg0.getKeyCode()==KeyEvent.VK_ESCAPE){
            rotarX=0;
            trasladaX=0;
            scaleX=3;
            scaleY=3;
            
        }
    }//fin keyPressed

     public void keyReleased(KeyEvent arg0){}
     public void keyTyped(KeyEvent arg0){}
}

