package org.yourorghere;

import com.sun.opengl.util.Animator;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;

import javax.swing.JFrame;
import com.sun.opengl.util.GLUT;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.media.opengl.glu.GLUquadric;



/**
 * ViewPortdoble.java <BR>
 * author: Brian Paul (converted to Java by Ron Cemer and Sven Goethel) <P>
 *
 * This version is equal to Brian Paul's version 1.2 1999/10/21
 */
public class ViewPortdoble extends JFrame implements KeyListener {
    
 public static GL gl;
    public static GLU glu;
    public static GLUT glut;
    public static GLCanvas canvas;
    public static int ancho,alto;
    private static float rotarX=0;
    private static float trasladaX=0;
    private static float scaleX=3,scaleY=3;
    
    
    public static void main(String[] args) {
        ViewPortdoble myframe = new ViewPortdoble();
         myframe.setVisible(true);
         myframe.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public ViewPortdoble(){
        setSize(700,600);
        setLocationRelativeTo(null);
        setTitle("Doble ViewPort");
        setResizable(false);
        GraphicListener listener = new GraphicListener();
        alto = this.getHeight();
        ancho = this.getWidth();
        canvas= new GLCanvas();
        gl=canvas.getGL();
        glu= new GLU();
        glut = new GLUT();
        canvas.addGLEventListener(listener);
        getContentPane().add(canvas);
        Animator animator = new Animator(canvas);
        animator.start();
        addKeyListener(this);
    }


    public class GraphicListener implements GLEventListener{
    public void display(GLAutoDrawable arg0) {
        
            gl=arg0.getGL();
            gl.glClear(GL.GL_COLOR_BUFFER_BIT);
            gl.glColor3f(1f, 1f, 1f);

            gl.glMatrixMode(GL.GL_PROJECTION);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);

            
            //dibuja cubo 
            gl.glPushMatrix();
            gl.glViewport(0,0, ancho, alto);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            gl.glColor3f(0,1,0);
            glut.glutWireCube(1);
            gl.glPopMatrix();
            //dibuja cubo 
            gl.glPushMatrix();
            gl.glViewport(0,400, 150, 150);
            gl.glLoadIdentity();
            gl.glOrtho(-5, 5, -5, 5, -5, 5);
            gl.glScalef(scaleX, scaleY, 3);
            gl.glTranslatef(trasladaX, 0, 0);
            gl.glRotatef(45,3,1,0);
            gl.glRotatef (rotarX,1,0,0);
            gl.glColor3f(0,1,0);
            glut.glutWireCube(1);
            gl.glPopMatrix();
             
             
            gl.glFlush();
            
    }

        public void init(GLAutoDrawable drawable) {
        }

        public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        }

        public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
        }
    }

     //Creacion del metodo que detecta las teclas pulsadas
    public void keyPressed(KeyEvent arg0){
        
        if(arg0.getKeyCode()==KeyEvent.VK_RIGHT){
            trasladaX+=.10f;
            System.out.println("El valor de Z: "+trasladaX);
        }

          if(arg0.getKeyCode()==KeyEvent.VK_LEFT){
            trasladaX-=.10f;
            System.out.println("El valor de Z: "+trasladaX);
        }
         
          if(arg0.getKeyCode()==KeyEvent.VK_A){
            rotarX+=1.0f;
            System.out.println("El valor de X en la rotacion: "+rotarX);
        }

        if(arg0.getKeyCode()==KeyEvent.VK_D){
            rotarX-=1.0f;
            System.out.println("El valor de X en la rotacion: "+rotarX);
        }

          if(arg0.getKeyCode()==KeyEvent.VK_J){
            scaleX+=.10f;
            System.out.println("El valor de Z: "+scaleX);
            scaleY+=.10f;
            System.out.println("El valor de Z: "+scaleY);
        }

          if(arg0.getKeyCode()==KeyEvent.VK_L){
            scaleX-=.10f;
            System.out.println("El valor de Z: "+scaleX);
            scaleY-=.10f;
            System.out.println("El valor de Z: "+scaleY);
        }
         

          if(arg0.getKeyCode()==KeyEvent.VK_ESCAPE){
            rotarX=0;
            trasladaX=0;
            scaleX=3;
            scaleY=3;
            
        }
    }//fin keyPressed
    
      public void keyTyped(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
    }
   
}

